const matches = require("../../data/matches.json");
const wonTossAndMatch = require("../functions/wonTossAndMatch.js");
const fs = require("file-system");

console.log(wonTossAndMatch(matches));
fs.writeFile("../../public/output/wonTossAndMatch.json", JSON.stringify(wonTossAndMatch(matches), null, 4), (err) => {
    if (err) {

        throw err;
    }
    console.log("wonTossAndMatch.json is saved.");
});