const deliveries = require("../../data/deliveries.json");
const playerDismissed = require("../functions/playerDismissed.js");
const fs = require("file-system");

console.log(playerDismissed(deliveries));
fs.writeFile("../../public/output/playerDismissed.json", JSON.stringify(playerDismissed(deliveries), null, 4), (err) => {
    if (err) {

        throw err;
    }
    console.log("playerDismissed.json is saved.");
});