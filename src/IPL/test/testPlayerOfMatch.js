const matches = require("../../data/matches.json");
const playerOfMatch = require("../functions/playerOfMatch.js");
const fs = require("file-system");

console.log(playerOfMatch(matches));
fs.writeFile("../../public/output/playerOfMatch.json", JSON.stringify(playerOfMatch(matches), null, 4), (err) => {
    if (err) {

        throw err;
    }
    console.log("playerOfMatch.json is saved.");
});