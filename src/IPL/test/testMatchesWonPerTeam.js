const matches = require("../../data/matches.json");
const matchesWonPerTeam = require("../functions/matchesWonPerTeam.js");
const fs = require("file-system");

console.log(matchesWonPerTeam(matches));
fs.writeFile("../../public/output/matchesWonPerTeam.json", JSON.stringify(matchesWonPerTeam(matches), null, 4), (err) => {
    if (err) {

        throw err;
    }
    console.log("matchesWonPerTeam.json is saved.");
});