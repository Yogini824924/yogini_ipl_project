const deliveries = require("../../data/deliveries.json");
const matches = require("../../data/matches.json");
const extraRunsPerTeam = require("../functions/extraRunsPerTeam2016.js");
const fs = require("file-system");
let year = 2016;

console.log(extraRunsPerTeam(matches, deliveries, year));
fs.writeFile("../../public/output/extraRunsPerTeam2016.json", JSON.stringify(extraRunsPerTeam(matches, deliveries, year), null, 4), (err) => {
    if (err) {

        throw err;
    }
    console.log("extraRunsPerTeam2016.json is saved.");
});