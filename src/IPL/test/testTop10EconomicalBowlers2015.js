const deliveries = require("../../data/deliveries.json");
const matches = require("../../data/matches.json");
const top10EconomicalBowlers = require("../functions/top10EconomicalBowlers2015.js");
const fs = require("file-system");
let year = 2015, top = 10;

console.log(top10EconomicalBowlers(matches, deliveries, year, top));
fs.writeFile("../../public/output/top10EconomicalBowlers2015.json", JSON.stringify(top10EconomicalBowlers(matches, deliveries, year, top), null, 4), (err) => {
    if (err) {

        throw err;
    }
    console.log("top10EconomicalBowlers2015.json is saved.");
});