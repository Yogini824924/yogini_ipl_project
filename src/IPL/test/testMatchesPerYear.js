const matches = require("../../data/matches.json");
const matchesPerYear = require("../functions/matchesPerYear.js");
const fs = require("file-system");

console.log(matchesPerYear(matches));
fs.writeFile("../../public/output/matchesPerYear.json", JSON.stringify(matchesPerYear(matches), null, 4), (err) => {
    if (err) {

        throw err;
    }
    console.log("matchesPerYear.json is saved.");
});