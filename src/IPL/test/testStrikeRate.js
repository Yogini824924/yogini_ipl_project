const deliveries = require("../../data/deliveries.json");
const matches = require("../../data/matches.json");
const strikeRate = require("../functions/strikeRate.js");
const fs = require("file-system");

console.log(strikeRate(matches, deliveries));
fs.writeFile("../../public/output/strikeRate.json", JSON.stringify(strikeRate(matches, deliveries), null, 4), (err) => {
    if (err) {

        throw err;
    }
    console.log("strikeRate.json is saved.");
});