const CSVtoJSON = require('csvtojson');
const fs=require('file-system');

CSVtoJSON().fromFile("/home/yogini/MountBlue/Yogini_IPL_Project/src/data/deliveries.csv")
.then(deliveries => { 
    fs.writeFile("/home/yogini/MountBlue/Yogini_IPL_Project/src/IPL/deliveries.json", JSON.stringify(deliveries, null, 4), (err) => {
        if (err) {

            throw err;
        }
        console.log("deliveries.json is saved.");
    })
});

CSVtoJSON().fromFile("/home/yogini/MountBlue/Yogini_IPL_Project/src/data/matches.csv")
.then(matches => {
    fs.writeFile("/home/yogini/MountBlue/Yogini_IPL_Project/src/IPL/matches.json", JSON.stringify(matches, null, 4), (err) => {
        if (err) {
            
            throw err;
        }
        console.log("matches.json is saved.");
    })
});