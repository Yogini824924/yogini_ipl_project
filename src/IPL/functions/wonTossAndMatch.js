//Find the number of times each team won the toss and also won the match

function wonTossAndMatch(matches) {

    if(!(Array.isArray(matches))) {

        return {};
    }
    let toss = {};
    for(let i = 0; i < matches.length; i++) {

        if(toss[matches[i]["toss_winner"]] === undefined) {

            if(matches[i]["toss_winner"]===matches[i]["winner"]) {

                toss[matches[i]["toss_winner"]] = 1;
            }
        }
        else {
            if(matches[i]["toss_winner"]===matches[i]["winner"]) {
                
                toss[matches[i]["toss_winner"]] += 1;
            }
        }
    }
    return toss;
}

module.exports = wonTossAndMatch;