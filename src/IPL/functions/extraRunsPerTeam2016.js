//Extra runs conceded per team in the year 2016

function extraRunsPerTeam(matches, deliveries, year) {

    if(!(Array.isArray(matches)) || !(Array.isArray(deliveries)) || !(typeof year === "number")) {

        return {};
    }
    let id = {},Runs = {},y = year.toString();
    for(let i = 0; i < matches.length; i++) {

        if(matches[i]["season"] === y) {

            id[matches[i]["id"]] = 1;
        }
    }
    for(let i = 0; i < deliveries.length; i++) {

        if(id[deliveries[i]["match_id"]] === 1) {

            if(Runs[deliveries[i]["bowling_team"]]) {

                Runs[deliveries[i]["bowling_team"]] += parseInt(deliveries[i]["extra_runs"]);
            }
            else {

                Runs[deliveries[i]["bowling_team"]] = parseInt(deliveries[i]["extra_runs"]);
            }
        }
    }
    return Runs;
}

module.exports = extraRunsPerTeam;