//Find the strike rate of a batsman for each season

function strikeRate(matches, deliveries) {

    if(!(Array.isArray(matches)) || !(Array.isArray(deliveries))) {

        return {};
    }
    let id = {};
    for(let i = 0; i < matches.length; i++) {

        id[matches[i]["id"]] = matches[i]["season"];
    }
    let rate = {};
    for(let i = 0; i < deliveries.length; i++) {

        if(rate[deliveries[i]["batsman"]] === undefined) {

            rate[deliveries[i]["batsman"]] = {};
        }
        if(rate[deliveries[i]["batsman"]][id[deliveries[i]["match_id"]]] === undefined) {

            rate[deliveries[i]["batsman"]][id[deliveries[i]["match_id"]]] = {};

        }
        if(rate[deliveries[i]["batsman"]][id[deliveries[i]["match_id"]]]["balls"] === undefined) {

            rate[deliveries[i]["batsman"]][id[deliveries[i]["match_id"]]]["balls"] = 1;
        }
        else {

            rate[deliveries[i]["batsman"]][id[deliveries[i]["match_id"]]]["balls"] += 1;
        }
        if(rate[deliveries[i]["batsman"]][id[deliveries[i]["match_id"]]]["runs"] === undefined) {

            rate[deliveries[i]["batsman"]][id[deliveries[i]["match_id"]]]["runs"] = parseInt(deliveries[i]["batsman_runs"]);
        }
        else {

            rate[deliveries[i]["batsman"]][id[deliveries[i]["match_id"]]]["runs"] += parseInt(deliveries[i]["batsman_runs"]);
        }
    }
    //return rate;
    for(let man in rate) {

        for(let year in rate[man]) {

            rate[man][year] = (parseFloat(rate[man][year]["runs"] / rate[man][year]["balls"])*100).toFixed(2);
        }
    }
    return rate;
}

module.exports = strikeRate;