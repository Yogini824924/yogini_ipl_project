//Number of matches played per year for all the years in IPL.

function matchesPerYear(matches) {

    if(!(Array.isArray(matches))) {

        return {};
    }
    let result = {};
    for(let i = 0; i < matches.length; i++) {

        if(result[matches[i]["season"]]) {

            result[matches[i]["season"]] += 1;
        }
        else {
            
            result[matches[i]["season"]] = 1;
        }
    }
    return result;
}

module.exports = matchesPerYear;