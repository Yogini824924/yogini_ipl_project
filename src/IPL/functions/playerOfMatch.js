//Find a player who has won the highest number of *Player of the Match* awards for each season

function playerOfMatch(matches) {

    if(!(Array.isArray(matches))) {

        return {};
    }
    let result = {};
    for(let i = 0; i < matches.length; i++) {

        if(result[matches[i]["season"]] === undefined) {

            result[matches[i]["season"]] = {};
        }
        if(result[matches[i]["season"]][matches[i]["player_of_match"]] === undefined) {

            result[matches[i]["season"]][matches[i]["player_of_match"]] = 1;
        }
        else {

            result[matches[i]["season"]][matches[i]["player_of_match"]] += 1;

        }
    }

    let players = {};
    for(let i in result) {

        let m = 0, k;
        for(let j in result[i]) {

            if(result[i][j] > m) {

                m = result[i][j];
                k = j;
            }
        }
        players[i] = k;
    }
    return players;
}

module.exports = playerOfMatch;