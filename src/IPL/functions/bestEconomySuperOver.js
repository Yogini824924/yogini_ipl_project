//Find the bowler with the best economy in super overs

function bestEconomySuperOver(deliveries) {

    if(!(Array.isArray(deliveries))) {

        return {};
    }
    let bowlers = {};
    for(let i = 0; i < deliveries.length; i++) {

        if(deliveries[i]["is_super_over"] === "1") {

            if(bowlers[deliveries[i]["bowler"]] === undefined) {

                bowlers[deliveries[i]["bowler"]] = {};
                bowlers[deliveries[i]["bowler"]]["balls"] = 1;
                bowlers[deliveries[i]["bowler"]]["runs"] = parseInt(deliveries[i]["total_runs"]);
            }
            else {

                bowlers[deliveries[i]["bowler"]]["balls"] += 1;
                bowlers[deliveries[i]["bowler"]]["runs"] += parseInt(deliveries[i]["total_runs"]);
            }
            bowlers[deliveries[i]["bowler"]]["economy"] = bowlers[deliveries[i]["bowler"]]["runs"] / (bowlers[deliveries[i]["bowler"]]["balls"] / 6);
        }
    }
    let economy =[];
    for(let k in bowlers) {

        bowlers[k]["economy"] = parseFloat(bowlers[k]["economy"].toFixed(2));
        economy.push(bowlers[k]["economy"]);
    }

    for(let i = 0; i < economy.length-1; i++) {

        for(let j = i+1; j < economy.length; j++) {

            if(economy[i] > economy[j]) {

                let a = economy[i];
                economy[i] = economy[j];
                economy[j] = a;

            }
        }
    }
    let result = {};
        for(let k in bowlers) {

            if(bowlers[k]["economy"] === economy[0]) {

                result[k] = bowlers[k]["economy"];
            }   
        }
    return result;
}

module.exports = bestEconomySuperOver;