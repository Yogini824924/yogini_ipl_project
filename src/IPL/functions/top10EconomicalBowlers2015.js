//Top 10 economical bowlers in the year 2015

function top10EconomicalBowlers(matches, deliveries, year, top) {
    
    if(!(Array.isArray(matches)) || !(Array.isArray(deliveries)) || !(typeof year === "number") || !(typeof top === "number")) {
        return {};
    }
    let id = {}, topEconomicBowlers = {}, yearToCompare = year.toString();

    for(let matchIndex = 0; matchIndex  < matches.length; matchIndex ++) {
        if(matches[matchIndex]["season"] === yearToCompare) {
            id[matches[matchIndex]["id"]] = 1;
        }
    }

    for(let deliveryIndex = 0; deliveryIndex < deliveries.length; deliveryIndex++) {
        if(id[deliveries[deliveryIndex]["match_id"]] === 1) {
            if(topEconomicBowlers[deliveries[deliveryIndex]["bowler"]] === undefined) {
                topEconomicBowlers[deliveries[deliveryIndex]["bowler"]] = {};
                topEconomicBowlers[deliveries[deliveryIndex]["bowler"]]["balls"] = 1;
                topEconomicBowlers[deliveries[deliveryIndex]["bowler"]]["runs"] = parseInt(deliveries[deliveryIndex]["total_runs"]);
            }
            else {
                topEconomicBowlers[deliveries[deliveryIndex]["bowler"]]["balls"] += 1;
                topEconomicBowlers[deliveries[deliveryIndex]["bowler"]]["runs"] += parseInt(deliveries[deliveryIndex]["total_runs"]);
            }

            topEconomicBowlers[deliveries[deliveryIndex]["bowler"]]["economy"] = topEconomicBowlers[deliveries[deliveryIndex]["bowler"]]["runs"] / (topEconomicBowlers[deliveries[deliveryIndex]["bowler"]]["balls"] / 6);
        }
    }

    let economy =[];

    for(let bowler in topEconomicBowlers) {
        topEconomicBowlers[bowler]["economy"] = parseFloat(topEconomicBowlers[bowler]["economy"].toFixed(2));
        economy.push(topEconomicBowlers[bowler]["economy"]);
    }

    for(let economyIndex = 0; economyIndex < economy.length-1; economyIndex++) {
        for(let economyIndexNext = economyIndex+1; economyIndexNext < economy.length; economyIndexNext++) {
            if(economy[economyIndex] > economy[economyIndexNext]) {
                let swap = economy[economyIndex];
                economy[economyIndex] = economy[economyIndexNext];
                economy[economyIndexNext] = swap;

            }
        }
    }

    let result = {};

    for(let resultIndex = 0; resultIndex < top; resultIndex++) {
        for(let bowler in topEconomicBowlers) {
            if(topEconomicBowlers[bowler]["economy"] === economy[resultIndex]) {
                result[bowler] = topEconomicBowlers[bowler]["economy"];
            }   
        }
    }

    return result;

}

module.exports = top10EconomicalBowlers;