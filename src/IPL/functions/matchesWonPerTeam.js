//Number of matches won per team per year in IPL.

function matchesWonPerTeam(matches) {

    if(!(Array.isArray(matches))) {
        return {};
    }

    let result = {};

    for(let i = 0; i < matches.length; i++) {
        if(matches[i]["result"] === "normal" || matches[i]["result"] === "tie") {
            if(result[matches[i]["winner"]] === undefined) {
                result[matches[i]["winner"]] = {};
            }

            if(result[matches[i]["winner"]][matches[i]["season"]] === undefined) {
                result[matches[i]["winner"]][matches[i]["season"]] = 1;
            }
            else {
                result[matches[i]["winner"]][matches[i]["season"]] += 1;
            }
        }
    }
    
    return result;
}

module.exports = matchesWonPerTeam;