//Find the highest number of times one player has been dismissed by another player

function playerDismissed(deliveries) {

    if(!(Array.isArray(deliveries))) {

        return {};
    }
    let players = {};
    for(let i = 0; i < deliveries.length; i++) {

        if(deliveries[i]["player_dismissed"] !== "") {

            if(players[deliveries[i]["player_dismissed"]] === undefined) {

                players[deliveries[i]["player_dismissed"]] = 1;
            }
            else {

                players[deliveries[i]["player_dismissed"]] += 1;

               /* if(deliveries[i]["fielder"] !== "") {

                    players[deliveries[i]["player_dismissed"]][deliveries[i]["fielder"]] += 1;
                }
                else {

                    players[deliveries[i]["player_dismissed"]][deliveries[i]["bowler"]] += 1;
                }*/
            }
        }
    }
    let m = 1, highest;
    for(let k in players) {

        if(m < players[k]) {

            m = players[k];
            highest = k;
        }
    }
    let obj = {};
    obj[highest] = m;
    return obj;
}

module.exports = playerDismissed;